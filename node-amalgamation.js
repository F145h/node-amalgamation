global.basePath = "./";

var reqBegin = "require(";
var reqEnd = ")";

if (process.argv.length!=4)
{
    console.log('Error: Invalid arguments.\nUse: node ./node-amalgamation.js <from> <to>');
    console.log('Example: node ./node-amalgamation.js main-file.js file-amalgama.js');
    return;
}

var from = process.argv[2];
var to = process.argv[3];

var fs = require('fs');

var fromContent = fs.readFileSync(from);

fs.writeFileSync(to, processJavascriptContent(fromContent.toString()), "utf8");



function processJavascriptContent(code)
{
    var result = String();

    var pos = 0;
    do {
        var blockBegin = code.indexOf(reqBegin, pos);
        if (blockBegin == -1)
            break;
        var pos = blockBegin;

        pos += reqBegin.length;
        blockNameEnd = code.indexOf(reqEnd, pos);
        if (blockNameEnd == -1)
            break;

        var filePath = code.substr(pos, blockNameEnd - pos);
        pos += filePath.length + reqEnd.length;

        var pathCalculated = eval(filePath.toString());

        if (fs.existsSync(pathCalculated))
        {
            result += code.substr(0, blockBegin);
            result += "new function () {";
            result += "    var module = {};";
            result += "    module.exports = {};";
            result += processJavascriptContent(fs.readFileSync(pathCalculated).toString());
            result += ";   return module.exports;";
            result += "}()";

            code = code.substr(pos);

            pos = 0;
        }
    }
    while (true);

    result += code;

    return result;
}